
/**
 * Escreva a descrição da classe Duck0 aqui.
 * 
 * @author Andreson Malaquias
 * @version 1.0
 */
public abstract class Duck4{
    FlyBehavior flyimp;
    QuackBehavior quackimp;
    
    public Duck4(){}

    public void setFlyBehavior(FlyBehavior fb){
        flyimp = fb;
    }
    
    public void setQuackBehavior(QuackBehavior qb){
        quackimp = qb;
    }
    
    public void swim()
    {
        System.out.println("...Todos os patos flutuam ...");
    }
    
    public void performFly(){
        flyimp.fly();
    }
    
    public void performQuack(){
        quackimp.quack();
    }

    abstract void display();
}
