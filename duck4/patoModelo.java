
/**
 * Escreva a descrição da classe patoModelo aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class patoModelo extends Duck4
{
   String nome_do_pato = "(sem nome)";
   
   public patoModelo(){
       flyimp = new FlyNoWay();
       quackimp = new Mudo();
   }
   
   public void setNome(String np){
       nome_do_pato = np;
   }
   
   public void display (){
       System.out.println("Display: " + nome_do_pato);
   }
}
