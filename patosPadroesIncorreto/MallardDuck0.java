
/**
 * Escreva a descrição da classe MallardDuck0 aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class MallardDuck0 extends Duck0 implements Flyable, Quackable
{
    public void display() {
        System.out.println("Display: Sou um pato MallarDuck");
    }
    public void fly() {
        System.out.println("Mode de voo 1");
    }
    public void quack() {
        System.out.println("Grasnar Mallar Duck 2: iiuiuiuiui");
    }
}
