
/**
 * Escreva a descrição da classe DecoyDuck1 aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class DecoyDuck1 extends Duck0
{
    public void display(){
        System.out.println("Display: pato madeira, so boiando");
    }
    public void quack(){
        System.out.println("Patos de borracha fazem: Squeak");
    };
    public void fly(){
        System.out.println("... patos de borracha não voam...");
    }
}
