import java.util.Observable;


public class CarroRoubado extends Observable implements Carro
{
    private String acao = "";
    
    public void andar(){
        acao = "frente";
        System.out.println("Carro roubado segue em frente");
        alertarObservador();
    }
    
    public void direita(){
        acao = "direita";
        System.out.println("Carro roubado segue na direita");
        alertarObservador();
    }
    
    public void esquerda(){
        acao = "esquerda";
        System.out.println("Carro roubado segue na esquerda");
        alertarObservador();
    }
    
    public void parar(){
        acao = "parou";
        System.out.println("Carro roubado parou");
        alertarObservador();
    }
    
    public void alertarObservador(){
        setChanged();
        notifyObservers(acao);
    }
}
