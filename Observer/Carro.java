
/**
 * Uma interface que irá implementar ações a subclasses
 * 
 * @author (Thallys) 
 * @version (número da versão ou data)
 */

public interface Carro
{
    public void andar();
    public void direita();
    public void esquerda();
    public void parar();
}
