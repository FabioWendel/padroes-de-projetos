import java.util.Observer;
import java.util.Observable;

public class CarroPolicia implements Observer, Carro
{
    public void andar(){
        System.out.println("Viatura segue em frente.");
    }
    
    public void direita(){
        System.out.println("Viatura vira na direita.");
    }
    
    public void esquerda(){
         System.out.println("Viatura vira na esquerda.");
    }
    
    public void parar(){
         System.out.println("Viatura parou");
    }
    
    public void update(Observable arg0, Object arg1){
        String acao = String.valueOf(arg1);
        
        if(acao.equals("frente")){
         this.andar();   
        }
        else if(acao.equals("direita")){
            this.direita();  
        }
        else if(acao.equals("esquerda")){
            this.esquerda();  
        }
        else if(acao.equals("parar")){
            this.parar();  
        }
    }
}
