
/**
 * Escreva a descrição da classe ReadHeadDuck0 aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class ReadHeadDuck0 extends Duck0 implements Flyable, Quackable
{
  public ReadHeadDuck0(){
       super();
    }
  public void display() {
       System.out.println("Display: mostrando um pato cabeça vermelha 2 ! ...");
    }
  public void fly() {
        System.out.println("Mode de voo 1");
    }
  public void quack() {
        System.out.println("Grasnar Mallar Duck 2: brobrobro gruumm");
    }
}
