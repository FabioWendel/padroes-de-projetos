import java.util.Observer;
import java.util.Observable;
import java.text.*;
import java.util.*;
/**
 * Escreva a descrição da classe Observador aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class Observador implements Observer
{
    DateFormat formatter = new SimpleDateFormat("dd-mm-yyyy H:mmm:ss");
    
    FormObservador df;
    
    public Observador( String nome) {
        df = new FormObservador(nome);
        Date agora = Calendar.getInstance().getTime();
        
        df.setTexto(
            "Observador criado \n"+"Agora : "+formatter.format(agora)+"\n"+"Nenhuma mensagem recebida do observador \n"
        );
    }
    
    public void update (Observable ob, Object msg ) {
        if(msg.toString().equals("limpa")){
            df.setTexto("limpa");
        }else {
            Date agora = Calendar.getInstance().getTime();
            df.setTexto(
                "Mensagem:"+msg.toString()+"\n"+"Hora da mensagem: "+formatter.format(agora)+"\n"+"Qtd de Observadores :"+ob.countObservers()+"\n --- \n"
            );
        }
    }
}
