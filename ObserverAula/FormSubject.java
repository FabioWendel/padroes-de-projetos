import java.util.Observer;

import java.util.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class FormSubject extends JFrame
{
    JTextField input = new JTextField(10);
    Subject subject = new Subject();
    
    public FormSubject(String nome)
    {
        JPanel panel = new JPanel();
        panel.add(new JLabel ("Enter: "));
        panel.add(input);
        
        input.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                subject.setMudanca(input.getText());}});
                
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                System.exit(0);}});
                
        getContentPane().add(panel);
        setTitle("< "+nome+" > SUBECT-Observado");
        setSize(300,80);
        setVisible(true);
    }
        public void setObservador(Observer obj)
        {
            subject.addObserver(obj);
        }
        
        public void deleteObservador(Observer obj)
        {
            subject.deleteObserver(obj);
        }
    
}