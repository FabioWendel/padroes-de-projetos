import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.text.*;
import java.util.*;

public class FormObservador
{
    Form ff;
    JFrame frame;
    
    public FormObservador( String nome)
    {
        frame = new JFrame("Observador");
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ff = new Form();
        frame.setTitle("< "+nome+" > observador");
        frame.add(ff);
        frame.pack();
        frame.setVisible(true);
    }
    
    public void setTexto(String texto) {
        if (texto.equals("limpa")){
            ff.textArea.setText("");
        }
        
        ff.textArea.append(texto);
    }
    
    public class Form extends JPanel {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy H:mm:ss");
        JTextArea textArea;
        
        public Form() {
            super(new GridBagLayout());
            
            textArea = new JTextArea(20, 25);
            textArea.setEditable(false);
            JScrollPane scrollPane = new JScrollPane(textArea);
            
            GridBagConstraints c = new GridBagConstraints();
            c.gridwidth = GridBagConstraints.REMAINDER;
            
            c.fill = GridBagConstraints.BOTH;
            c.weightx = 1.0;
            c.weighty = 1.0;
            add(scrollPane, c);
        }
    }
}
