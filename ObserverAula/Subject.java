import java.util.Observable;

public class Subject extends Observable
{
    private String atualiz;
    
    public void setMudanca(String at)
    {
        atualiz = at;
        setChanged();
        notifyObservers(atualiz);
    }
}
