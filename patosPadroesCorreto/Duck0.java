
/**
 * Classe abstrata Duck0 - escreva a descrição da classe aqui
 * 
 * @author (seu nome aqui)
 * @version (versão ou data)
 */
public abstract class Duck0
{
    FlyBehavior flyimp;
    QuackBehavior quackimp;
    
    public Duck0(){    
    }
    
    public void swim(){
       System.out.println("Todos os patos flutuam");
    }
        
    abstract void display();
    
    public void performfly(){
      flyimp.fly();
    }
    
    public void performQuack(){
       quackimp.quack();
    }
}
